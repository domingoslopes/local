console.log("Hello World" , "!");
console.log(nome);

var nome = "valor";

const nomeConst = {
    "nome":"Marcos",
    "idade": 29 
}

console.log(nomeConst );

function somar(valor1, valor2 = 1){
    console.log(valor1 + valor2);
}

somar(3);
somar(3,2);

function ondeMoro(cidade){
    console.log("Eu moro em " + cidade + " e sou feliz");

}

function fruteira(){
    let texto = "Banana"
                + "\n"
                + "Ameixa"
                + "\n"
                + "Ameixa"
                + "\n"


    console.log(texto);
}

fruteira();

let funcaoSomarValores = function(a,b){
    return a + b;
}

const eu = {
    "nome":"Domingos",
    "idade":22,
    "altura": 100
}

function testarPessoa({nome, idade, altura}){
    console.log( nome, idade, altura);
}

testarPessoa(eu);
